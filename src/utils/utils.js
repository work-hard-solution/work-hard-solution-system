/* eslint-disable array-callback-return */
const mongoose = require('mongoose');
const crypto = require('crypto');

// Constants
const { CODE_LENGTH, RANGE_NUMBER_CODE } = require('./constants');

const algorithm = 'aes-256-ctr';
const secretKey = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3';
const iv = crypto.randomBytes(16);

/**
 * Transform a Mongoose document to JSON object
 */
exports.transformMongooseDocumentToObject = (doc) => {
    if (!doc) return doc;
    const { _id, __v, ...rest } = doc;
    return { id: _id, ...rest };
};

/**
 * Lookup collection
 * @param {Date} from
 * @param {String} refFrom
 * @param {String} refTo
 * @param {String} select
 * @param {String} reName
 */
exports.$lookup = (from, refFrom, refTo, select, reName, conditions) => ({
    $lookup: {
        from,
        let: {
            [refTo]: `$${refTo}`,
        },
        pipeline: [
            {
                $match: {
                    $expr: {
                        $eq: [`$${refFrom}`, `$$${refTo}`],
                    },
                    ...conditions,
                },
            },
            {
                $project: {
                    _id: 0,
                    id: '$_id',
                    ...select.split(/[,\s]/).reduce((r, rs) => {
                        r[rs] = 1;
                        return r;
                    }, {}),
                },
            },
        ],
        as: reName,
    },
});

/**
 * Regrex search
 * @param {*} q
 * @param {*} fields
 */
exports.regrexSearch = (q, fields) =>
    q &&
    fields.split(/[,\s]/).reduce((r, rs) => {
        r[rs] = new RegExp(q, 'gi');
        return r;
    }, {});

/**
 * Pagination
 * @param {Object} $match
 * @param {Array} pipeline
 */
exports.pagination = ($match, pipeline, lookups, conditions) => {
    // Convert _id to ObjectId
    Object.keys($match).map((key) => {
        $match[key] = /_id/.test(key) ? mongoose.Types.ObjectId($match[key]) : $match[key];
    });
    const query = [
        {
            $match,
        },
    ];
    if (lookups) lookups.map((lookup) => query.push(lookup));
    if (conditions) query.push(conditions);
    query.push({
        $facet: {
            total_count: [
                {
                    $count: 'total_count',
                },
            ],
            items: pipeline,
        },
    });
    query.push({ $project: { result: { $setUnion: ['$total_count', '$items'] } } });
    query.push({ $unwind: '$result' });
    query.push({ $replaceRoot: { newRoot: '$result' } });
    return query;
};

/**
 * Default query
 * @param {*} $match
 * @param {*} pipeline
 */
exports.query = ($match, pipeline) => {
    // eslint-disable-next-line array-callback-return
    Object.keys($match).map((key) => {
        $match[key] = /_id/.test(key) ? mongoose.Types.ObjectId($match[key]) : $match[key];
    });
    return [
        {
            $match,
        },
        ...pipeline,
    ];
};

/**
 * Gen number code
 */
exports.genSecretCode = () =>
    [...Array(CODE_LENGTH).keys()]
        // eslint-disable-next-line no-unused-vars
        .map(() => {
            const index = Math.floor(Math.random() * RANGE_NUMBER_CODE.length);
            return RANGE_NUMBER_CODE[index];
        })
        .join('');

/**
 * Encrypt
 * @param {any} text
 */
exports.encrypt = (text) => {
    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);

    const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);

    return {
        iv: iv.toString('hex'),
        content: encrypted.toString('hex'),
    };
};

/**
 * Decrypt
 * @param {String} hash
 */
exports.decrypt = (hash) => {
    const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, 'hex'));

    const decrypted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);

    return decrypted.toString();
};
