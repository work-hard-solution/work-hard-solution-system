module.exports = {
    OAUTH2: {
        GOOGLE: 'google',
        LINKEDIN: 'linkedin',
        GITHUB: 'github',
    },

    // Gen code
    CODE_LENGTH: 6,
    RANGE_NUMBER_CODE: '0123456789',
    RANGE_CHAR_CODE: 'ABCEGJKLMNPQRVWXYZ',

    // Redis
    REDIS_EXP_SECRET: 300,

    // Limit size of image uploaded (5MB) (5 * 1024 * 1024)
    LIMIT_IMAGE_SIZE: 5242880,
    // Aws-s3
    CACHE_CONTROL: 'public, max-age=31536000',
    ACL: 'public-read',

    // Google
    GOOGLE_USER_PROFILE: 'https://oauth2.googleapis.com/tokeninfo?id_token=',

    // Linkedin
    LINKEDIN_USER_PROFILE:
        'https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))',
    LINKEDIN_USER_EMAIL: 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))',

    // Github
    GITHUB_USER_EMAIL: 'https://api.github.com/user/emails',
    GITHUB_USER_PROFILE: 'https://api.github.com/user',

    // Stripe
    STRIPE_BASE_URL: 'https://api.stripe.com',
    STRIPE_APP_VERSION: '2020-08-27',

    // PAGINATION
    PAGINATION: {
        PAGE: 1,
        PER_PAGE: 10,
        SORT_BY: 'created_at',
        ORDER_BY: 1,
    },
    // Object id pattern
    OBJECT_ID_PATTERN: /^[0-9a-fA-F]{24}$/,
    USER_TYPES: {
        ADMIN: 'admin',
        USER: 'user',
    },
    USER_STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive',
    },
    USER_IDENTITIES: {
        GOOGLE: 'google',
        LINKEDIN: 'linkedin',
        GITHUB: 'github',
    },
    // Card status
    CARD_STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive',
    },

    // Client status
    CLIENT_STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive',
    },
    // Organization status
    ORGANIZATION_STATUS: {
        ACTIVE: 'active',
        TRIAL: 'trial',
        ARCHIVE: 'archive',
        SUSPENDED: 'suspended',
    },

    // Project status
    PROJECT_STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive',
    },

    // Task status
    TASK_STATUS: {
        OPEN: 'Open',
        IN_PROCESS: 'In_Progress',
        TEST: 'Test',
        DONE: 'Done',
    },

    // TEMPLATE NAME
    TEMPLATE_NAME: {
        INVITE_TO_ORGANIZATION: 'Invite To Organization',
        RESET_PASSWORD: 'Reset Password',
        VERIFY_ACCOUNT: 'Verify Email',
        CONFIRM_PASSWORD_CHANGED: 'Confirm Password Changed',
        NOTIFICATION: 'Notification',
    },

    TEMPLATE_PARAMS: {
        INVITE_TO_ORGANIZATION: {
            INVITER: 'inviter',
            INVITED_ORGANIZATION: 'invited_organization',
            INVITE_URL: 'invite_url',
        },
        RESET_PASSWORD: {
            USER_FULLNAME: 'user_fullname',
            RESET_PASSWORD_URL: 'reset_password_url',
            RESET_PASSWORD_CODE: 'reset_password_code',
        },
        VERIFY_ACCOUNT: {
            USER_FULLNAME: 'user_fullname',
            VERIFY_URL: 'verify_url',
        },
        CONFIRM_PASSWORD_CHANGED: {
            USER_FULLNAME: 'user_fullname',
        },
        NOTIFICATION: {
            USER_FULLNAME: 'user_fullname',
            REMOVER: 'remover',
        },
    },
    // SUBJECT EMAIL
    SUBJECT: {
        INVITE_MEMBER: 'Invite member',
        FORGOT_PASSWORD: 'Forgot password',
        VERIFY_ACCOUNT: 'Verify account',
        CONFIRM_PASSWORD_CHANGED: 'Confirm Password Changed',
        NOTIFICATION: 'Notification',
    },

    // TYPE CODE
    CODE: {
        VERIFY_ACCOUNT: 'verify_account',
        FORGOT_PASSWORD: 'forgot_password',
    },

    AGENDA_JOB: {
        SYNC_SUBSCRIPTION_DATA: 'Sync subscription data',
        SEND_EMAIL: 'Send email',
        UPDATE_SUBSCRIPTION: 'update subscription',
        CANCEL_SUBSCRIPTION: 'cancel subscription',
    },
};
