const moment = require('moment-timezone');

/**
 * Validate timezone
 * @param {String} timezone
 * @returns Boolean
 */
exports.validateTimezone = async (timezone) => moment.tz.names().includes(timezone);

/**
 *
 * @param {ISOdate} fromTime
 * @param {ISOdate} toTime
 */
exports.getRangeDates = async (fromTime, toTime) => {
    const rangeDate = [];
    let i;
    for (
        i = moment(fromTime).toDate();
        moment(i).toDate() < moment(toTime).endOf('day').toDate();
        i = moment(i).add(1, 'd').toDate()
    ) {
        rangeDate.push(moment(i).toDate());
    }
    return rangeDate;
};
