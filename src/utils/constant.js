module.exports = {
    // COMMON
    ROLES: {
        ORGANIZATION_OWNER: 'organization_owner',
        ORGANIZATION_MANAGER: 'organization_manager',
        ORGANIZATION_MEMBER: 'organization_member',
        ORGANIZATION_GUEST: 'organization_guest',
        PROJECT_MANAGER: 'project_manager',
        PROJECT_MEMBER: 'project_member',
        PROJECT_VIEWER: 'project_viewer',
        ADMIN: 'admin',
    },
    objectIdPattern: /^[0-9a-fA-F]{24}$/,
    CONCURRENCY: {
        VND: 'vnd',
        USD: 'usd',
        EUR: 'eur',
        GBP: 'gbp',
        CAD: 'cad',
    },
    STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive',
        PENDING_DELETE: 'pending delete',
    },

    OAUTH2: {
        GOOGLE: 'google',
        GITHUB: 'github',
        LINKEDIN: 'linked_in',
    },
    USER_PROFILE: {
        URL_GOOGLE: 'https://oauth2.googleapis.com/tokeninfo?id_token=',
        URL_BASIC_INFO_LINKED_IN:
            'https://api.linkedin.com/v2/me?projection=(id,firstName,lastName,profilePicture(displayImage~:playableStreams))',
        URL_EMAIL_LINK_IN: 'https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))',
        GITHUB_USER_EMAIL: 'https://api.github.com/user/emails',
        GITHUB_USER_PROFILE: 'https://api.github.com/user',

        /**
         * STATE_LOGIN_AUTH
         */
        STATE_LOGIN_AUTH: 'security_token%3D138r5719ru3e1%26url%3Dhttps%3A%2F%2Foauth2.example.com%2Ftoken',

        // GOOGLE
        GOOGLE_AUTHORIZATION_URL: 'https://accounts.google.com/o/oauth2/v2/auth',
        GOOGLE_TOKEN_URL: 'https://oauth2.googleapis.com/token',

        // LINKED IN
        LINKED_IN_AUTHORIZATION_URL: 'https://www.linkedin.com/oauth/v2/authorization',
        LINKED_IN_TOKEN_URL: 'https://www.linkedin.com/oauth/v2/accessToken',

        // GITHUB
        GITHUB_AUTHORIZATION_URL: 'https://github.com/login/oauth/authorize',
        GITHUB_TOKEN_URL: 'https://github.com/login/oauth/access_token',
    },
    // CLIENT
    CLIENT_STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive',
    },

    // INVITE_STATUS
    INVITE_STATUS: {
        SENT: 'sent',
        ACCEPTED: 'accepted',
        CANCELLED: 'cancelled',
    },

    // SUBSCRIPTION
    SUBSCRIPTION_STATUS: {
        FUTURE: 'future',
        ACTIVE: 'active',
        IN_TRIAL: 'in_trial',
        NON_RENEWING: 'non-renewing',
        PAUSED: 'paused',
        CANCELED: 'cancelled',
    },

    EVENT_TYPE: {
        SUBSCRIPTION_STARTED: 'subscription_started',
        SUBSCRIPTION_RENEWED: 'subscription_renewed',
        SUBSCRIPTION_TRIAL_END_REMINDER: 'subscription_trial_end_reminder',
        SUBSCRIPTION_ACTIVATED: 'payment_succeeded',
        SUBSCRIPTION_PAUSED: 'subscription_paused',
        SUBSCRIPTION_RESUMED: 'subscription_resumed',
        SUBSCRIPTION_CANCELLED: 'subscription_cancelled',
    },

    SUBSCRIPTION_BILLING_CYCLE: {
        WEEKLY: 'Weekly',
        MONTHLY: 'Monthly',
        YEARLY: 'Yearly',
    },

    // USER
    USER_TYPES: {
        ADMIN: 'admin',
        USER: 'user',
    },
    ORGANIZATION_STATUS: {
        ACTIVE: 'active',
        TRIAL: 'trial',
        ARCHIVE: 'archive',
        PENDING_DELETE: 'pending delete',
        DELETED: 'deleted',
    },
    USER_STATUS: {
        ACTIVE: 'active',
        INACTIVE: 'inactive',
    },
    USER_IDENTITIES: {
        GOOGLE: 'google',
        LINKEDIN: 'linkedin',
        GITHUB: 'github',
    },
    // TRANSACTION
    TRANSACTION_STATUS: {
        SUCCESS: 'success',
        FAIL: 'fail',
    },

    // TIMESLOT
    TIMESLOT_TYPE: {
        AUTO: 'auto',
        MANUAL: 'manual',
    },
    // TASK
    TASK_STATUS: {
        OPEN: 'Open',
        IN_PROCESS: 'In_Progress',
        TEST: 'Test',
        DONE: 'Done',
    },

    TYPE_REPORT: { WORK_SUMMARY: 'work-summary', DETAIL_USER_PROJECT: 'detail', TIME_SHEET: 'time-sheet' },
    //
    INVITE_TOKEN_STATUS: {
        DELETED: 'deleted',
    },
    // PAYROLL
    PAYROLL_STATUS: {
        PENDING: 'pending',
        PROCESSING: 'processing',
        ERROR: 'error',
        PARIALLY: 'parially',
        PAID: 'paid',
    },

    // PAGINATION
    PAGINATION: {
        PAGE: 1,
        PER_PAGE: 10,
        SORT_BY: 'created_at',
        ORDER_BY: 1,
    },

    AGENDA_JOB: {
        SYNC_SUBSCRIPTION_DATA: 'Sync subscription data',
        SEND_EMAIL: 'Send email',
        UPDATE_SUBSCRIPTION: 'update subscription',
        CANCEL_SUBSCRIPTION: 'cancel subscription',
        DELETE_PROJECT_JOB: 'Delete project',
    },
    // GEN CODE
    CODE_LENGTH: 6,
    RANGE_NUMBER_CODE: '0123456789',
    RANGE_CHAR_CODE: 'ABCEGJKLMNPQRVWXYZ',

    // REDIS
    REDIS_EXP_SECRET: 300,
    REDIS_KEY: {
        RESET_PASSWORD: 'reset_password',
        VERIFY_ACCOUNT: 'verify_account',
    },

    PAY_TYPE: { HOURLY: 'hourly', FIXED_AMOUNT: 'fixed_amount' },
    PAY_PERIOD: {
        NONE: 'none',
        WEEKLY: 'weekly',
        TWICE_PER_MONTH: 'twice_per_month',
        BI_WEEKLY: 'bi_weekly',
        MONTHLY: 'monthly',
    },

    ACTION_REACH_LIMIT: { STOP_WORKING: 'Stop working', NOTHING: 'Nothing' },

    // TEMPLATE NAME
    TEMPLATE_NAME: {
        INVITE_TO_ORGANIZATION: 'invite-to-organization',
        RESET_PASSWORD: 'reset-password',
        VERIFY_ACCOUNT: 'verify-email',
        CONFIRM_PASSWORD_CHANGED: 'confirm-password-changed',
        SEND_REPORT_EMAIL: 'send-report-email',
        DELETE_ORGANIZATION_TO_MANAGER: 'delete-organization-tomanager',
        DELETE_ORGANIZATION_TO_MEMBER: 'delete-organization-tomember',
        DELETE_PROJECT_TO_MANAGER: 'delete-project-tomanager',
        DELETE_PROJECT_TO_MEMBER: 'delete-project-tomember',
        REMOVE_ORGANIZATION_MEMBER: 'remove-organization-member',
        REMOVE_PROJECT_MEMBER: 'remove-project-member',
    },

    TEMPLATE_PARAMS: {
        INVITE_TO_ORGANIZATION: {
            INVITER: 'inviter',
            INVITED_ORGANIZATION: 'invited-organization',
            INVITE_URL: 'invite-url',
        },
        RESET_PASSWORD: {
            USER_FULLNAME: 'user-fullname',
            RESET_PASSWORD_URL: 'reset-password-url',
            RESET_PASSWORD_CODE: 'reset-password-code',
        },
        VERIFY_ACCOUNT: {
            USER_FULLNAME: 'user-fullname',
            VERIFY_URL: 'verify-url',
        },
        CONFIRM_PASSWORD_CHANGED: {
            USER_FULLNAME: 'user-fullname',
        },
        SEND_REPORT_EMAIL: {
            SENDER_NAME: 'sender-name',
            REPORT_NAME: 'report-name',
            ADDED_MESSAGE: 'added-message',
        },
        DELETE_ORGANIZATION_TO_MANAGER: {
            USER_FULLNAME: 'user-fullname',
            ORGANIZATION_NAME: 'organization-name',
            ORGANIZATION_SHORT_NAME: 'organization-short-name',
        },
        DELETE_ORGANIZATION_TO_MEMBER: {
            USER_FULLNAME: 'user-fullname',
            ORGANIZATION_NAME: 'organization-name',
            ORGANIZATION_SHORT_NAME: 'organization-short-name',
        },
        DELETE_PROJECT_TO_MANAGER: {
            USER_FULLNAME: 'user-fullname',
            PROJECT_NAME: 'project-name',
        },
        DELETE_PROJECT_TO_MEMBER: {
            USER_FULLNAME: 'user-fullname',
            PROJECT_NAME: 'project-name',
        },
        REMOVE_ORGANIZATION_MEMBER: {
            USER_FULLNAME: 'user-fullname',
            ORGANIZATION_NAME: 'organization-name',
        },
        REMOVE_PROJECT_MEMBER: {
            USER_FULLNAME: 'user-fullname',
            PROJECT_NAME: 'project-name',
        },
    },
    // SUBJECT EMAIL
    SUBJECT: {
        INVITE_MEMBER: 'Invite member',
        FORGOT_PASSWORD: 'Timebee account recovery instructions',
        VERIFY_ACCOUNT: 'Verify account',
        CONFIRM_PASSWORD_CHANGED: 'Your Timebee account password has been changed',
        NOTIFICATION: 'Notification',
    },

    // TYPE CODE
    CODE: {
        VERIFY_ACCOUNT: 'verify_account',
        FORGOT_PASSWORD: 'forgot_password',
    },

    LEVEL_ROLE: {
        ADMIN: 5,
        ORGANIZATION_OWNER: 4,
        ORGANIZATION_MANAGER: 3,
        ORGANIZATION_MEMBER: 1,
        PROJECT_MANAGER: 2,
        PROJECT_MEMBER: 1,
        ORGANIZATION_GUEST: 0,
        PROJECT_VIEWER: 0,
    },
    FILE_FORMAT: {
        PDF: 'pdf',
        CSV: 'csv',
        EXCEL: 'excel',
    },
    WEEK_START_ON: {
        MONDAY: 'monday',
        TUESDAY: 'tuesday',
        WEDNESDAY: 'wednesday',
        THURSDAY: 'thursday',
        FRIDAY: 'friday',
        SATURDAY: 'saturday',
        SUNDAY: 'sunday',
    },
    INDUSTRY: {
        ACCOUNTING: 'accounting',
        AGENCY: 'agency',
        DIGITAL_MARKETING: 'digital marketing',
        IT_SERVICES: 'it services',
        FREELANCING: 'freelancing',
        ENTERTAINMENT: 'entertainment',
        SOFTWARE: 'software',
        E_COMMERCE: 'eCommerce',
    },
    REDIS_DURATION: 28800,
};
