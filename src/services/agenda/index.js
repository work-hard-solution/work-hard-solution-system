/* eslint-disable func-names */
const Agenda = require('agenda');
const { MONGO } = require('../../config/config');
// const { AGENDA_JOB } = require('../../utils/constants');
const logger = require('../../core/logger');

const agenda = new Agenda({
    db: {
        address: MONGO.URI,
        collection: 'agenda_jobs',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        },
    },
});

module.exports = agenda;
async function graceful() {
    await agenda.stop();
    process.exit(0);
}

process.on('SIGTERM', graceful);
process.on('SIGINT', graceful);

module.exports.run = () => {
    agenda.on('ready', async function () {
        await agenda.start();
        logger.info('✅ Agenda started');
    });
};
