const puppeteer = require('puppeteer');

exports.PDFService = async (htmlString) => {
    /**
     * Convert HTML string to PDF
     * @param htmlString - HTML as string
     * @return {Buffer} PDF buffer
     */
    try {
        const browser = await puppeteer.launch({
            headless: true,
            executablePath: process.env.CHROME_BIN || null,
            args: ['--no-sandbox', '--headless', '--disable-gpu', '--disable-dev-shm-usage'],
        });
        const page = await browser.newPage();
        await page.setContent(htmlString);
        const pdfBuffer = await page.pdf({
            format: 'A4',
            margin: { left: '1cm', top: '1cm', right: '1cm', bottom: '1cm' },
        });
        await browser.close();
        return pdfBuffer;
    } catch (err) {
        return Buffer.from('PDF error!');
    }
};
