/* eslint-disable no-return-await */
const mailgun = require('mailgun-js')({
    apiKey: process.env.MAIL_GUN_API_KEY,
    domain: process.env.MAIL_GUN_DOMAIN,
});

/**
 * Send email with template
 * @param {String} templateName
 * @param {Object} user
 * @param {Object} messageConditions
 */
exports.sendEmailWithTemplate = async ({ from, templateName, user, attachment, messageConditions }) => {
    // const { from, templateName, user, messageConditions } = job.attrs.data;
    const data = {
        from,
        to: user.email || null,
        subject: messageConditions.subject,
        template: templateName,
        attachment: attachment || null,
        'h:X-Mailgun-Variables': messageConditions.mailgunVariables,
    };
    return await asyncSendEmail(data);
};

// Convert callback to async await
const asyncSendEmail = (...args) =>
    new Promise((resolve, reject) => {
        mailgun.messages().send(...args, (err, body) => {
            if (body) return resolve(body);
            return reject(err);
        });
    });
