// eslint-disable-next-line import/no-unresolved
const mandrill = require('mandrill-api/mandrill');

const { MANDRILL_API_KEY } = require('../../config/config');

// Setup client
const mandrillClient = new mandrill.Mandrill(MANDRILL_API_KEY);

/**
 * Send email with template
 * @param {String} templateName
 * @param {String} templateContent
 * @param {Object} user
 * @param {Object} messageConditions
 */
exports.sendEmailWithTemplate = async ({ templateName, templateContent, user, messageConditions }) => {
    // Set up message
    const message = {
        html: messageConditions.html || null,
        text: messageConditions.text || null,
        subject: messageConditions.subject,
        to: [
            {
                email: user.email,
                name: user.name,
                type: messageConditions.type || 'to',
            },
        ],
        important: false,
        auto_text: null,
        auto_html: null,
        inline_css: null,
        url_strip_qs: null,
        preserve_recipients: null,
        view_content_link: null,
        merge: true,
        merge_language: 'mailchimp',
        merge_vars: messageConditions.merge_vars,
        tags: [messageConditions.tags] || null,
        attachments: messageConditions.attachments || null,
        images: messageConditions.images || null,
    };

    // Send email
    await asyncMandrillSend({
        template_name: templateName,
        template_content: templateContent,
        message,
        async: false,
        ip_pool: messageConditions.ip_pool || 'Main Pool',
    });
};

// Convert callback to async await
const asyncMandrillSend = (...args) =>
    new Promise((resolve, reject) => {
        mandrillClient.messages.sendTemplate(
            ...args,
            (result) => resolve(result),
            (err) => reject(err),
        );
    });
