// eslint-disable-next-line no-unused-vars
const httpStatus = require('http-status');
const axios = require('axios');
const { STRIPE_PUBLISHABLE_KEY } = require('../../../../config/config');
const { STRIPE_APP_VERSION, STRIPE_BASE_URL } = require('../../../../utils/contants');
const { AppError } = require('../../../error');
const AuthError = require('../../../../api/auth/auth.error');

// eslint-disable-next-line import/order
const stripe = require('stripe')(STRIPE_PUBLISHABLE_KEY, {
    apiVersion: STRIPE_APP_VERSION,
});

// /**
//  * @param {*} userInfo
//  */
// exports.createCustomer = async (userInfo) => {
//     try {
//         const { email, name } = userInfo;

//         const customer = await stripe.customers.create({ name: name.full_name, email });
//         if (!customer.id) throw new Error('Cant create customer');
//         const intent = await stripe.setupIntents.create({
//             customer: customer.id,
//         });

//         return { userInfo, client_secret: intent.client_secret, customer: customer.id };
//     } catch (e) {
//         if (e.raw) {
//             // eslint-disable-next-line no-throw-literal
//             throw {
//                 status: e.statusCode,
//                 message: e.raw.message,
//             };
//         }
//         // eslint-disable-next-line no-throw-literal
//         throw {
//             status: httpStatus.INTERNAL_SERVER_ERROR,
//             message: 'Internal error',
//         };
//     }
// };

// /**
//  * @param {*} userInfo
//  * @param {*} paymentInfo
//  */
// exports.createCustomerDuringPayment = async (userInfo, paymentInfo) => {
//     try {
//         const customer = await stripe.customers.create(userInfo);
//         if (!customer) throw new Error('Cant create customer');
//         const intent = await stripe.paymentIntents.create({
//             ...paymentInfo,
//             customer: customer.id,
//         });
//         return { userInfo, client_secret: intent.client_secret, customer: customer.id };
//     } catch (e) {
//         if (e.raw) {
//             // eslint-disable-next-line no-throw-literal
//             throw {
//                 status: e.statusCode,
//                 message: e.raw.message,
//             };
//         }
//         // eslint-disable-next-line no-throw-literal
//         throw {
//             status: 500,
//             message: 'Internal error',
//         };
//     }
// };

// /**
//  * @param {*} customer
//  * @param {*} type
//  */
// exports.getAllPaymentOfCustomer = async (customer, type) => {
//     try {
//         const paymentMethods = await stripe.paymentMethods.list({
//             customer,
//             type,
//         });
//         return paymentMethods;
//     } catch (e) {
//         if (e.raw) {
//             // eslint-disable-next-line no-throw-literal
//             throw {
//                 status: e.statusCode,
//                 message: e.raw.message,
//             };
//         }
//         // eslint-disable-next-line no-throw-literal
//         throw {
//             status: 500,
//             message: 'Internal error',
//         };
//     }
// };

// /**
//  * @param {*} param0
//  */
// exports.payCreditCard = async ({ amount, currency, customer, paymentMethod }) => {
//     try {
//         try {
//             const paymentIntent = await stripe.paymentIntents.create({
//                 amount,
//                 currency,
//                 customer,
//                 paymentMethod,
//                 off_session: true,
//                 confirm: true,
//             });
//             return paymentIntent;
//         } catch (err) {
//             // Error code will be authentication_required if authentication is needed
//             const paymentIntentRetrieved = await stripe.paymentIntents.retrieve(err.raw.payment_intent.id);
//             return paymentIntentRetrieved;
//         }
//     } catch (e) {
//         if (e.raw) {
//             // eslint-disable-next-line no-throw-literal
//             throw {
//                 status: e.statusCode,
//                 message: e.raw.message,
//             };
//         }
//         // eslint-disable-next-line no-throw-literal
//         throw {
//             status: 500,
//             message: 'Internal error',
//         };
//     }
// };

exports.getAllSubscription = async () => {
    const subscriptions = await axios.get(`${STRIPE_BASE_URL}/v1/subscriptions`);
    if (!subscriptions) throw new AppError(AuthError.InvalidToken);
    return subscriptions;
};

// const endpoint = await stripe.webhookEndpoints.create({
//     url: 'https://example.com/my/webhook/endpoint',
//     enabled_events: ['charge.failed', 'charge.succeeded'],
// });
