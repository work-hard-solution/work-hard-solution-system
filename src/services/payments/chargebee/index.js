const chargebee = require('chargebee');
const { CHARGEBEE_API_KEY, CHARGEBEE_SITE } = require('../../../config/config');

// Config
chargebee.configure({ site: CHARGEBEE_SITE, api_key: CHARGEBEE_API_KEY });
module.exports = chargebee;
