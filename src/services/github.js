// const axios = require('axios');
// const { AppError } = require('../error');
// const AuthError = require('../../api/auth/auth.error');
// const { GITHUB_USER_PROFILE } = require('../../utils/constants');

// /**
//  * @param {String} token
//  */
// exports.getUserProfileFromGitHub = async (token) => {
//     try {
//         const headers = {
//             Authorization: `Bearer ${token}`,
//         };
//         const { data } = await axios.get(GITHUB_USER_PROFILE, { headers });
//         // On github user can set 'Dont show email'
//         // Response's github have 2 email
//         if (!data || data[0].email) throw new AppError(AuthError.InvalidToken);
//         return data[0];
//     } catch (error) {
//         throw new AppError(AuthError.InvalidAccessToken);
//     }
// };
