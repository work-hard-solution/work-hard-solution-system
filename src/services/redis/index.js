const Redis = require('ioredis');
const _ = require('lodash');
const { REDIS_URL } = require('../../config/config');

let redis;
if (!redis) {
    redis = new Redis(REDIS_URL);
}

const del = async (key) => redis.del(key);

module.exports.del = del;

module.exports.keys = async (key) => redis.keys(key);

module.exports.hgetall = async (key) => redis.hgetall(key);

module.exports.hmget = async (...args) => redis.hmget(args);

module.exports.hget = async (...args) => redis.hget(args);

module.exports.hmset = async (...args) => redis.hmset(args);

module.exports.lpop = async (...args) => redis.lpop(args);

module.exports.lpush = async (...args) => redis.lpush(args);

module.exports.lindex = async (...args) => redis.lindex(args);

module.exports.get = async (key) => redis.get(key);

module.exports.setObjectToRedis = async (key, object, expTimeAsSeconds) => {
    await del(key);

    if (!_.isString(object)) object = JSON.stringify(object);

    await redis.set(key, object);
    if (expTimeAsSeconds) {
        await redis.expire(key, expTimeAsSeconds);
    }
};

module.exports.getObjectFromRedis = async (key) => {
    const val = await redis.get(key);
    if (!val) return null;
    try {
        return JSON.parse(val);
    } catch (e) {
        return null;
    }
};

module.exports.setKeyToRedis = async (key, field, value, expTime) => {
    await redis.del(key);
    await redis.hmset(key, field, value);
    if (expTime) {
        await redis.expire(key, expTime);
    }
};

module.exports.getKeyByPattern = async (key) => redis.keys(`${key}*`);
