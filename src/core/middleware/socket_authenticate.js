const jwt = require('jsonwebtoken');
const _ = require('lodash');
const { AppError, errors } = require('../error');
const { JWT_SECRET } = require('../../config/config');

exports.isAuthenticatedSocket = (socket, next) => {
    try {
        // Get access token from Headers
        const accessToken = socket.handshake.query.access_token;
        // Verify access token
        const { sub, exp, iat, ...restPayload } = jwt.verify(accessToken, JWT_SECRET);
        // Set decoded payload to req.auth
        socket.auth = { id: sub, ...restPayload };
        return next();
    } catch (err) {
        if (_.isEqual(err.name, 'TokenExpiredError')) return next(new AppError(errors.AccessTokenExpired));
        if (_.isEqual(err.name, 'JsonWebTokenError')) return next(new AppError(errors.InvalidAccessToken));
        return next(err);
    }
};
