// Library
const _ = require('lodash');
const moment = require('moment-timezone');

// Model
const OrganizationService = require('../../api/organization/organization.service');
const ProjectService = require('../../api/project/project.service');
const ClientService = require('../../api/client/client.service');

// Constants
const { ORGANIZATION_STATUS } = require('../../utils/constants');

// Error
const { AppError } = require('../error');
const OrganizationError = require('../../api/organization/organization.error');

module.exports = async (req, res, next) => {
    try {
        // Get organization id
        const params = { ...req.params, ...req.headers };

        // Get organization
        const organization = await OrganizationService.getOrganizationById(params.organization_id);

        if (_.isNull(organization)) {
            throw new AppError(OrganizationError.OrganizationNotFound);
        }

        // Check expires trial organization
        if (
            _.isEqual(organization.status, ORGANIZATION_STATUS.TRIAL) &&
            moment(organization.expires_trial).subtract(new Date()) <= 0
        ) {
            // Update organization status to suspended
            await Promise.all([
                OrganizationService.changeStatusOrganization(organization.id, ORGANIZATION_STATUS.SUSPENDED),
                ProjectService.suspendedAllProjectOrganization(organization.id),
                ClientService.suspendedAllClientOrganization(organization.id),
            ]);
            // TODO: When suspended organization
            // TODO: Set expires time screenshot on awsS3
            // TODO: Send email notification your organization suspended
            throw new AppError(OrganizationError.TheOrganizationIsSuspended);
        }

        req.org = organization;
        return next();
    } catch (err) {
        return next(err);
    }
};
