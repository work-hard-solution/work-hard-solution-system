const mongoose = require('mongoose');
const { pick } = require('lodash');

/**
 * AuthSession schema
 * @private
 */
const Schema = new mongoose.Schema(
    {
        // The user ID
        user_id: { type: String, required: true },
        // IP location (e.g. 127.0.0.1)
        ip: { type: String },
        // Device (e.g. Safari on MacOS)
        device: { type: String },
        // The refresh token, which can be used to obtain new access token
        refresh_token: { type: String },
        // The session (refresh token) expires
        expires: { type: Date, expires: 0 },
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } },
);

/**
 * Methods
 */
Schema.method({
    /**
     * Transform to JSON
     * @param {Array} select - Selected fields
     */
    transform(select = ['id', 'user_id', 'ip', 'device', 'refresh_token', 'expires', 'created_at', 'updated_at']) {
        return pick(this, select);
    },
});

/**
 * Statics
 */
Schema.statics = {
    /**
     * Override "findById"
     * @override
     */
    findById(id) {
        if (!mongoose.Types.ObjectId.isValid(id)) return undefined;
        return this.findOne({ _id: id });
    },
};

/**
 * Module exports
 * @public
 */
module.exports = mongoose.model('AuthSession', Schema, 'auth_sessions');
