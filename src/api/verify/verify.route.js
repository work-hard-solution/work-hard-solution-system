const controller = require('./verify.controller');
const validation = require('./verify.validation');
const authenticate = require('../../core/middleware/authenticate');

/**
 * Module exports
 * @public
 */
module.exports = [
  /** Request verify account */
  {
    path: '/request-verify-account',
    method: 'get',
    middlewares: [authenticate],
    controller: controller.requestVerifyAccount,
  },
  {
    path: '/verify-account',
    method: 'get',
    middlewares: [validation.verifyAccount],
    controller: controller.verifyAccount,
  },
];
