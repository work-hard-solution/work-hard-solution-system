const httpStatus = require('http-status');
const path = require('path');
const VerifyService = require('./verify.service');

/**
 * Request verify account
 */
exports.requestVerifyAccount = async (req, res, next) => {
  try {
    const user = req.auth;
    await VerifyService.requestVerifyAccount(user.id, user.email);
    return res.status(httpStatus.NO_CONTENT).json();
  } catch (err) {
    return next(err);
  }
};

/** Verify account */
exports.verifyAccount = async (req, res, next) => {
  try {
    const { verify_code: verifyCode } = req.query;

    await VerifyService.verifyAccount(verifyCode);
    return res.render(path.join(__dirname, '../../public/views/verify-account.ejs'));
  } catch (err) {
    return next(err);
  }
};
