/* eslint-disable no-return-await */
// Library
const _ = require('lodash');
const moment = require('moment-timezone');
const ms = require('ms');
const { v4: uuidV4 } = require('uuid');
const { Duration, DateTime } = require('luxon');

// Model
const VerifyModel = require('mongoose').model('Verify');

// Services
const UserService = require('../user/user.service');
const AuthService = require('../auth/auth.service');
const { sendEmailWithTemplate } = require('../../services/mandrill');
// Error
const { AppError } = require('../../core/error');
const UserError = require('../user/user.error');
const VerifyError = require('./verify.error');

// Constants
const { APP_HOST, VERIFY_ACCOUNT_TOKEN } = require('../../config/config');
const { SUBJECT, CODE, TEMPLATE_NAME, TEMPLATE_PARAMS } = require('../../utils/constants');

const verifyTokenDuration = Duration.fromMillis(ms(VERIFY_ACCOUNT_TOKEN));

const verifyTransform = ['id', 'user_email', 'type', 'code', 'expires'];

exports.getVerifyByCode = async (conditions) =>
    VerifyModel.findOne({
        user_email: conditions.user_email,
        type: conditions.type,
        code: conditions.code,
    });

/**
 * Get verify by code
 * @param {String} code
 * @param {String} type
 */
const getVerify = async (code, type) => {
    const verify = await VerifyModel.findOne({
        code,
        type,
    });
    if (_.isNull(verify)) {
        throw new AppError(VerifyError.VerifyNotFound);
    }
    // Transform verify
    const transformedVerify = _.pick(verify, verifyTransform);
    return transformedVerify;
};
exports.getVerify = getVerify;

/**
 * Get verify by email
 * @param {String} userEmail
 * @param {String} type
 */

exports.getVerifyByEmail = async (userEmail, type) =>
    await VerifyModel.findOne({
        user_email: userEmail,
        type,
    });

/**
 * Create verify
 * @param {Object} conditions
 */
exports.createVerify = async (conditions) => {
    const verify = await VerifyModel.findOne({
        user_email: conditions.user_email,
        type: conditions.type,
    });

    if (!_.isNull(verify)) {
        throw new AppError(VerifyError.VerifyAlreadyExists);
    }
    const newVerify = await VerifyModel.create({
        ...conditions,
    });

    // Transform verify
    const transformedVerify = _.pick(newVerify, verifyTransform);
    return transformedVerify;
};
/**
 * Request verify account
 * @param {String} userId
 * @param {String} userEmail
 */
exports.requestVerifyAccount = async (userId, userEmail) => {
    const user = await UserService.getUserByEmail(userId, userEmail);
    if (_.isNull(user)) {
        throw new AppError(UserError.UserNotFound);
    }

    if (_.isEqual(user.email_verified, true)) {
        throw new AppError(VerifyError.YouAlreadyVerifiedAccount);
    }
    const verify = await VerifyModel.findOne({ user_email: user.email, type: CODE.VERIFY_ACCOUNT });

    if (!_.isNull(verify)) {
        if (moment(new Date()).subtract(30, 'seconds') < verify.updated_at) {
            throw new AppError(VerifyError.YouAlreadySendEmailPleaseResendAfter30Seconds);
        }
        // Gen verify code
        const verifyCode = uuidV4().replace(/-/g, '');

        // Send email
        sendEmailWithTemplate({
            templateName: TEMPLATE_NAME.VERIFY_ACCOUNT,
            templateContent: [{}],
            user: {
                email: verify.user_email,
            },
            messageConditions: {
                subject: SUBJECT.VERIFY_ACCOUNT,
                merge_vars: [
                    {
                        rcpt: verify.user_email,
                        vars: [
                            {
                                name: TEMPLATE_PARAMS.VERIFY_ACCOUNT.USER_FULLNAME,
                                content: user.name,
                            },
                            {
                                name: TEMPLATE_PARAMS.VERIFY_ACCOUNT.VERIFY_URL,
                                content: `${APP_HOST}:5000/v1/verify-account?verify_code=${verifyCode}`,
                            },
                        ],
                    },
                ],
            },
        });
        // eslint-disable-next-line no-return-await
        return await VerifyModel.findOneAndUpdate(
            {
                _id: verify._id,
                user_email: verify.user_email,
                type: CODE.VERIFY_ACCOUNT,
            },
            {
                $set: {
                    code: verifyCode,
                    expires: DateTime.utc().plus(verifyTokenDuration).toJSON(),
                },
            },
            {
                new: true,
            },
        );
    }
    // Gen verify token
    const newVerify = await VerifyModel.create({
        user_email: user.email,
        type: CODE.VERIFY_ACCOUNT,
        code: uuidV4().replace(/-/g, ''),
        expires: DateTime.utc().plus(verifyTokenDuration).toJSON(),
    });

    // Send email
    sendEmailWithTemplate({
        templateName: TEMPLATE_NAME.VERIFY_ACCOUNT,
        templateContent: [{}],
        user: {
            email: user.email,
        },
        messageConditions: {
            subject: SUBJECT.VERIFY_ACCOUNT,
            merge_vars: [
                {
                    rcpt: user.email,
                    vars: [
                        {
                            name: TEMPLATE_PARAMS.VERIFY_ACCOUNT.USER_FULLNAME,
                            content: user.name,
                        },
                        {
                            name: TEMPLATE_PARAMS.VERIFY_ACCOUNT.VERIFY_URL,
                            content: `${APP_HOST}:5000/v1/verify-account?verify_code=${newVerify.code}`,
                        },
                    ],
                },
            ],
        },
    });
};

/**
 * Verify account
 * @param {String} verifyToken
 */
exports.verifyAccount = async (verifyCode) => {
    const verify = await getVerify(verifyCode, CODE.VERIFY_ACCOUNT);

    // Check exists verify
    if (_.isNull(verify)) {
        throw new AppError(VerifyError.VerifyNotFound);
    }

    // Verify account
    await Promise.all([
        AuthService.verifyAccount(verify.user_email),
        VerifyModel.findOneAndDelete({
            type: CODE.VERIFY_ACCOUNT,
            code: verifyCode,
        }),
    ]);
};

/**
 * Forgot password flow
 * @param {Object} conditions
 */
exports.forgotPassword = async (conditions) => {
    await VerifyModel.findOneAndUpdate(
        {
            _id: conditions.id,
            user_email: conditions.user_email,
        },
        {
            $set: {
                code: conditions.code,
            },
        },
        {
            new: true,
        },
    );
};

/**
 * Clear verify after verify done
 * @param {Object} conditions
 */
exports.clearVerify = async (conditions) => {
    await VerifyModel.findOneAndDelete({
        _id: conditions._id,
        user_email: conditions.user_email,
        type: conditions.type,
    });
};
