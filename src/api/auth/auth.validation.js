const { validate, Joi, Segments } = require('../../core/validation');

module.exports = {
    register: validate({
        [Segments.BODY]: Joi.object({
            first_name: Joi.string().min(2).max(128).required(),
            last_name: Joi.string().min(2).max(128).required(),
            email: Joi.string().email().max(128).required(),
            password: Joi.string()
                .max(128)
                .regex(/^(?=.*[a-zA-Z])(?=.*[0-9]).{6,}$/)
                .required(),
            username: Joi.string()
                .regex(/^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/)
                .max(128),
            type: Joi.string(),
        }),
    }),
    login: validate({
        [Segments.BODY]: Joi.object({
            id: Joi.string().required(),
            password: Joi.string().required(),
        }),
    }),
    loginByOauth2: validate({
        [Segments.BODY]: Joi.object({
            token: Joi.string().required(),
        }),
    }),
    refresh: validate({
        [Segments.BODY]: Joi.object({
            refresh_token: Joi.string().required(),
        }),
    }),
};
