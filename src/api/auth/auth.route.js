const controller = require('./auth.controller');
const validation = require('./auth.validation');

/**
 * Module exports
 * @public
 */
module.exports = [
    /** Register new user */
    {
        path: '/auth/register',
        method: 'post',
        middlewares: [validation.register],
        controller: controller.register,
    },
    /** Login with email and password */
    {
        path: '/auth/login',
        method: 'post',
        middlewares: [validation.login],
        controller: controller.login,
    },
    /** Login with Oauth2 */
    {
        path: '/auth/login/:type',
        method: 'post',
        middlewares: [validation.loginByOauth2],
        controller: controller.loginByOAuth2,
    },
    /** Refresh token */
    {
        path: '/auth/refresh-token',
        method: 'post',
        middlewares: [validation.refresh],
        controller: controller.refresh,
    },
    // {
    //     path: '/test/stripe',
    //     method: 'get',
    //     controller: controller.getAllSubscription,
    // },
];
