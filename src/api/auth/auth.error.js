const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */
module.exports = Object.freeze({
    IncorrectIdOrPassword: {
        message: 'Incorrect id or password',
        code: 0,
        status: httpStatus.UNAUTHORIZED,
        isPublic: true,
    },
    EmailAlreadyTaken: {
        message: 'This email already exists',
        code: 0,
        status: httpStatus.CONFLICT,
        isPublic: true,
    },
    InvalidRefreshToken: {
        message: 'Invalid or expired refresh token',
        code: 0,
        status: httpStatus.UNAUTHORIZED,
        isPublic: true,
    },
    InvalidToken: {
        message: 'Invalid or expired token',
        code: 0,
        status: httpStatus.UNAUTHORIZED,
        isPublic: true,
    },
});
