const httpStatus = require('http-status');

const AuthService = require('./auth.service');

const GoogleServices = require('../../services/google');
const GithubServices = require('../../services/github');
// const StripeServices = require('../../core/services/payments/stripe/stripe');

const { OAUTH2 } = require('../../utils/constants');
/**
 * Get client info
 *
 * @param {Request} request - Express request
 * @return {Object}
 * @private
 */
const getClientInfo = (request) => {
    const { headers, client } = request;
    // Get client agent (e.g. Opera/9.60 (Windows NT 6.0; U; en) Presto/2.1.1)
    const clientAgent = headers['user-agent'];
    // Get client IP (e.g. 127.0.0.1)
    const clientIP =
        (headers['X-Forwarded-For'] || headers['x-forwarded-for'] || '').split(',')[0] || client.remoteAddress;
    return {
        ip: clientIP,
        agent: clientAgent,
    };
};

/**
 * Register controller
 */
exports.register = async (req, res, next) => {
    try {
        const client = getClientInfo(req);
        const { user, tokens } = await AuthService.register({ user: req.body, client });
        return res.status(httpStatus.CREATED).json({ user, tokens });
    } catch (err) {
        return next(err);
    }
};

/**
 * Refresh token controller
 */
exports.refresh = async (req, res, next) => {
    try {
        const { refresh_token: refreshToken } = req.body;
        const [userID] = refreshToken.split('#');
        const client = getClientInfo(req);
        const tokens = await AuthService.refresh({ userID, refreshToken, client });
        return res.status(httpStatus.OK).json(tokens);
    } catch (err) {
        return next(err);
    }
};

/**
 * Login controller
 */
exports.login = async (req, res, next) => {
    try {
        const { id, password } = req.body;
        const client = getClientInfo(req);
        const { user, tokens } = await AuthService.login({ id, password, client });
        return res.status(httpStatus.OK).json({ user, tokens });
    } catch (err) {
        return next(err);
    }
};

/**
 * Login by Oauth2
 */
exports.loginByOAuth2 = async (req, res, next) => {
    try {
        let userInfo;
        switch (req.params.type) {
            case OAUTH2.GOOGLE:
                userInfo = await GoogleServices.getUserProfileFromGoogle(req.body.token);
                break;
            case OAUTH2.GITHUB:
                userInfo = await GithubServices.getUserProfileFromGitHub(req.body.token);
                break;
            default:
                break;
        }
        const client = getClientInfo(req);
        const { user, tokens } = await AuthService.loginByOAuth2(userInfo, client);
        return res.status(httpStatus.OK).json({ user, tokens });
    } catch (err) {
        return next(err);
    }
};
