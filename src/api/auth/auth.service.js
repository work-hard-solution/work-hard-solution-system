// Library
const bcrypt = require('bcryptjs');
const _ = require('lodash');
const moment = require('moment-timezone');
const ms = require('ms');
const jwt = require('jsonwebtoken');
const { v4: uuidV4 } = require('uuid');
const { Duration, DateTime } = require('luxon');

// Model
const UserModel = require('mongoose').model('User');
const AuthSessionModel = require('mongoose').model('AuthSession');

// Services
const VerifyService = require('../verify/verify.service');
const SendEmailWithTemplate = require('../../services/mandrill');

// Error
const { AppError, errors } = require('../../core/error');
const UserError = require('../user/user.error');
const AuthError = require('./auth.error');
const VerifyError = require('../verify/verify.error');

// Constants
const Utils = require('../../utils/utils');
const { ACCESS_TOKEN_EXP, REFRESH_TOKEN_EXP, JWT_SECRET, FORGOT_PASSWORD_CODE_EXP } = require('../../config/config');
const { SUBJECT, STATUS, CODE, TEMPLATE_NAME, TEMPLATE_PARAMS } = require('../../utils/constants');

const accessTokenDuration = Duration.fromMillis(ms(ACCESS_TOKEN_EXP));
const refreshTokenDuration = Duration.fromMillis(ms(REFRESH_TOKEN_EXP));
const forgotPasswordDuration = Duration.fromMillis(ms(FORGOT_PASSWORD_CODE_EXP));

const userTransform = ['id', 'name', 'email', 'email_verified', 'picture', 'type', 'status', 'created_at'];

/**
 * Hash user password
 *
 * @param {String} pwd - Password
 * @return {Promise<string>} Hashed password
 * @private
 */
const hashPassword = async (pwd) => bcrypt.hash(pwd, 12);

/**
 * Verify user password
 *
 * @param {String} pwd - Password
 * @param {String} hash - Hashed password
 * @return {Boolean}
 * @private
 */
const verifyPassword = async (pwd, hash) => {
    const result = await bcrypt.compare(pwd, hash);
    return !!result;
};

/**
 * Generate Bearer tokens | Include "access token" and "refresh token"
 *
 * @param {Object} user - User info
 * @private
 */
const generateTokens = (user) => {
    const accessToken = jwt.sign(
        {
            sub: user.id,
            name: user.name,
            email: user.email,
            email_verified: user.email_verified,
            type: user.type,
            exp: DateTime.utc().plus(accessTokenDuration).toSeconds(),
        },
        JWT_SECRET,
    );
    const refreshToken = `${user.id}#${uuidV4().replace(/-/g, '')}`;
    return {
        token_type: 'Bearer',
        access_token: accessToken,
        expires_in: accessTokenDuration.as('seconds'),
        refresh_token: refreshToken,
    };
};

/**
 * Generate new auth session
 *
 * @param {Object} user - User info
 * @param {Object} client - Client info
 * @private
 */
const generateAuthSession = async ({ user, client }) => {
    const tokens = generateTokens(user);
    const authSessionRecord = await AuthSessionModel.create({
        user_id: user.id,
        ip: client.ip,
        device: client.agent,
        refresh_token: tokens.refresh_token,
        expires: DateTime.utc().plus(refreshTokenDuration).toJSON(),
    });
    const authSession = authSessionRecord.transform();
    return {
        tokens,
        session: authSession,
    };
};

/**
 * Register a new user
 * @param {Object} user - User info
 * @param {Object} client - Client info
 * @public
 */
exports.register = async ({ user, client }) => {
    // Check duplicate user email
    if (await UserModel.countDocuments({ email: user.email })) {
        throw new AppError(AuthError.EmailAlreadyTaken);
    }
    // Hash password
    const hashedPwd = await hashPassword(user.password);
    // Create new user
    const newUser = await UserModel.create({
        ...user,
        first_name: user.first_name,
        last_name: user.last_name,
        name: `${user.first_name} ${user.last_name}`,
        username: uuidV4().replace(/-/g, ''),
        password: hashedPwd,
    });
    const transformedNewUser = _.pick(newUser, userTransform);

    // Create new session
    const { tokens } = await generateAuthSession({ user: transformedNewUser, client });
    return { user: transformedNewUser, tokens };
};

/**
 * Login with email and password
 *
 * @param {String} id - User email or username
 * @param {String} password - User password
 * @param {Object} client - Client info
 * @public
 */
exports.login = async ({ id, password, client }) => {
    // Get existing user by email or username
    const user = await UserModel.getByEmailOrUserName(id);
    // Check user password
    if (!user || !(await verifyPassword(password, user.password))) {
        throw new AppError(AuthError.IncorrectIdOrPassword);
    }
    const transformedUser = _.pick(user, userTransform);
    // Create new session
    const { tokens } = await generateAuthSession({ user: transformedUser, client });
    return { user: transformedUser, tokens };
};

/**
 * Refresh token
 *
 * @param {string} userID - User ID
 * @param {String} refreshToken - Refresh token
 * @param {Object} client - Client info
 * @public
 */
exports.refresh = async ({ userID, refreshToken, client }) => {
    // Find user and auth session
    const [userRecord, authSessionRecord] = await Promise.all([
        UserModel.findById(userID),
        AuthSessionModel.findOne({ refresh_token: refreshToken }),
    ]);
    // Check valid token
    if (!userRecord || !authSessionRecord || DateTime.fromISO(authSessionRecord.expires) < DateTime.utc()) {
        throw new AppError(AuthError.InvalidRefreshToken);
    }
    // Transform user
    const user = userRecord.transform();
    // Create new session
    const tokens = await generateTokens(user);
    // Update auth session
    authSessionRecord.ip = client.ip;
    authSessionRecord.client = client.agent;
    authSessionRecord.refresh_token = tokens.refresh_token;
    authSessionRecord.expires = DateTime.utc().plus(refreshTokenDuration).toJSON();
    await authSessionRecord.save();
    return tokens;
};

/**
 * Forgot password
 * @param {String} userEmail
 */
exports.forgotPassword = async (userEmail) => {
    const user = await UserModel.findOne({ email: userEmail });

    // Check exists email
    if (_.isNull(user)) {
        throw new AppError(UserError.UserNotFound);
    }

    const verify = await VerifyService.getVerifyByEmail(userEmail, CODE.FORGOT_PASSWORD);
    if (!_.isNull(verify)) {
        if (moment(new Date()).subtract(30, 'seconds') < verify.updated_at) {
            throw new AppError(VerifyError.YouAlreadySendEmailPleaseResendAfter30Seconds);
        }

        // Gen code
        const code = await Utils.genSecretCode();

        // eslint-disable-next-line no-return-await
        return await Promise.all([
            // Send email
            SendEmailWithTemplate({
                templateName: TEMPLATE_NAME.RESET_PASSWORD,
                templateContent: [{}],
                user: {
                    email: verify.user_email,
                },
                messageConditions: {
                    subject: SUBJECT.FORGOT_PASSWORD,
                    merge_vars: [
                        {
                            rcpt: verify.user_email,
                            vars: [
                                {
                                    name: TEMPLATE_PARAMS.RESET_PASSWORD.USER_FULLNAME,
                                    content: user.name,
                                },
                                {
                                    name: TEMPLATE_PARAMS.RESET_PASSWORD.RESET_PASSWORD_URL,
                                    content: `http://localhost:3000/reset-password?code=${code}&email=${user.email}`,
                                },
                                {
                                    name: TEMPLATE_PARAMS.RESET_PASSWORD.RESET_PASSWORD_CODE,
                                    content: `${code}`,
                                },
                            ],
                        },
                    ],
                },
            }),
            // Update record verify
            VerifyService.forgotPassword({
                id: verify.id,
                user_email: verify.user_email,
                type: CODE.FORGOT_PASSWORD,
                code,
                expires: DateTime.utc().plus(forgotPasswordDuration).toJSON(),
            }),
        ]);
    }

    // Gen new code
    const newCode = await Utils.genSecretCode();

    await Promise.all([
        VerifyService.createVerify({
            user_email: user.email,
            type: CODE.FORGOT_PASSWORD,
            code: newCode,
            expires: DateTime.utc().plus(forgotPasswordDuration).toJSON(),
        }),
        // Send email
        SendEmailWithTemplate({
            templateName: TEMPLATE_NAME.RESET_PASSWORD,
            templateContent: [{}],
            user: {
                email: user.email,
            },
            messageConditions: {
                subject: SUBJECT.FORGOT_PASSWORD,
                merge_vars: [
                    {
                        rcpt: user.email,
                        vars: [
                            {
                                name: TEMPLATE_PARAMS.RESET_PASSWORD.USER_FULLNAME,
                                content: user.name,
                            },
                            {
                                name: TEMPLATE_PARAMS.RESET_PASSWORD.RESET_PASSWORD_URL,
                                content: `http://localhost:3000/reset-password?code=${newCode}&email=${user.email}`,
                            },
                            {
                                name: TEMPLATE_PARAMS.RESET_PASSWORD.RESET_PASSWORD_CODE,
                                content: `${newCode}`,
                            },
                        ],
                    },
                ],
            },
        }),
    ]);
};

/**
 * Reset password
 * @param {String} code
 * @param {String} newPassword
 * @param {String} userEmail
 */
exports.resetPassword = async (code, newPassword, userEmail) => {
    const user = await UserModel.findOne({ email: userEmail });

    // Check exists email
    if (_.isNull(user)) {
        throw new AppError(UserError.UserNotFound);
    }

    const verify = await VerifyService.getVerifyByCode({
        user_email: userEmail,
        type: CODE.FORGOT_PASSWORD,
        code,
    });

    // Verify code
    if (_.isNull(verify)) {
        throw new AppError(errors.InvalidCode);
    }

    if (user.password && (await verifyPassword(newPassword, user.password))) {
        throw new AppError(AuthError.NewPasswordBeTheSameAsOldPassword);
    }
    // Hash new password
    const newPasswordHash = await hashPassword(newPassword);

    // Change new password and delete key at Redis
    await Promise.all([
        UserModel.findOneAndUpdate(
            {
                email: userEmail,
                status: STATUS.ACTIVE,
            },
            {
                $set: {
                    password: newPasswordHash,
                },
            },
            { new: true },
        ),
        VerifyService.clearVerify({
            _id: verify._id,
            user_email: user.email,
            type: CODE.FORGOT_PASSWORD,
        }),
    ]);

    // Send email confirm
    SendEmailWithTemplate({
        templateName: TEMPLATE_NAME.CONFIRM_PASSWORD_CHANGED,
        templateContent: [{}],
        user: {
            email: user.email,
            name: user.name,
        },
        messageConditions: {
            subject: SUBJECT.CONFIRM_PASSWORD_CHANGED,
            merge_vars: [
                {
                    rcpt: user.email,
                    vars: [
                        {
                            name: TEMPLATE_PARAMS.CONFIRM_PASSWORD_CHANGED.USER_FULLNAME,
                            content: user.name,
                        },
                    ],
                },
            ],
        },
    });
};

/**
 * Change password
 * @param {String} userEmail
 * @param {String} currentPassword
 * @param {String} newPassword
 */
exports.changePassword = async (userEmail, currentPassword, newPassword) => {
    const user = await UserModel.findOne({
        email: userEmail,
        status: STATUS.ACTIVE,
    });

    // Check exists user
    if (_.isNull(user)) {
        throw new AppError(UserError.UserNotFound);
    }

    if (!(await verifyPassword(currentPassword, user.password))) {
        throw new AppError(AuthError.IncorrectPassword);
    }

    // New password can not be the same old password
    if (await verifyPassword(newPassword, user.password)) {
        throw new AppError(AuthError.NewPasswordBeTheSameAsOldPassword);
    }
    // Hash new password
    const newPasswordHash = await hashPassword(newPassword);

    await UserModel.findOneAndUpdate(
        {
            email: user.email,
            status: STATUS.ACTIVE,
        },
        {
            $set: {
                password: newPasswordHash,
            },
        },
        {
            new: true,
        },
    );
    // Send email
    SendEmailWithTemplate({
        templateName: TEMPLATE_NAME.CONFIRM_PASSWORD_CHANGED,
        templateContent: [{}],
        user: {
            email: user.email,
            name: user.name,
        },
        messageConditions: {
            subject: SUBJECT.CONFIRM_PASSWORD_CHANGED,
            merge_vars: [
                {
                    rcpt: user.email,
                    vars: [
                        {
                            name: TEMPLATE_PARAMS.CONFIRM_PASSWORD_CHANGED.USER_FULLNAME,
                            content: user.name,
                        },
                    ],
                },
            ],
        },
    });
};
