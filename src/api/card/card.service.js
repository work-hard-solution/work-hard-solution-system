// Library
const _ = require('lodash');
const Card = require('mongoose').model('Card');

// Config
const { CHARGEBEE_GATEWAY_ACCOUNT_ID } = require('../../config/config');

// Services
const chargebee = require('../../services/payments/chargebee');

// Errors
const CardError = require('./card.error');
const { AppError } = require('../../core/error');

// Utils
const { transformMongooseDocumentToObject: transform } = require('../../utils/utils');

exports.createCard = async (stripeToken, user, isPrimary) => {
    const [existCard, countCard] = await Promise.all([
        Card.findOne({ stripeToken }),
        Card.countDocuments({ owner_id: user.id }),
    ]);

    // Check exists card
    if (!_.isNull(existCard)) {
        throw new AppError(CardError.CardAlreadyExists);
    }

    // Create customer on chargebee
    const customerChargebee = await chargebee.customer.create({ last_name: user.name, email: user.email }).request();

    // Create card on chargebee
    const cardChargebee = await chargebee.payment_source
        .create_using_temp_token({
            tmp_token: stripeToken,
            customer_id: customerChargebee.customer.id,
            gateway_account_id: CHARGEBEE_GATEWAY_ACCOUNT_ID, // Stripe gateway id . Get on dashboard {{site}}.chargebee.com
            type: 'card',
        })
        .request();

    if (isPrimary && countCard > 0) await Card.update({ owner_id: user.id }, { $set: { is_primary: false } });

    const cardDTO = {
        owner_id: user.id,
        customer_ref_id: customerChargebee.customer.id,
        gateway_account_id: CHARGEBEE_GATEWAY_ACCOUNT_ID,
        reference_id: cardChargebee.payment_source.reference_id,
        payment_method: cardChargebee.payment_source.card.brand,
        stripeToken,
        last_number: cardChargebee.payment_source.card.last4,
        exp_month: cardChargebee.payment_source.card.expiry_month,
        exp_year: cardChargebee.payment_source.card.expiry_year,
        billing_zip: cardChargebee.payment_source.card.billing_zip,
    };

    // Create new card
    const [card] = await Card.create([{ ...cardDTO, is_primary: countCard === 0 ? true : cardDTO.is_primary }]);

    return transform(card.toObject());
};
