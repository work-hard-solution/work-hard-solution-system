const httpStatus = require('http-status');
const CardService = require('./card.service');

/** Create new card */
exports.createCard = async (req, res, next) => {
    try {
        const { stripeToken, is_primary: isPrimary } = req.body;
        const card = await CardService.createCard(stripeToken, req.auth, isPrimary);
        return res.status(httpStatus.CREATED).json(card);
    } catch (err) {
        return next(err);
    }
};
