const controller = require('./card.controller');
const validation = require('./card.validation');
const authenticate = require('../../core/middleware/authenticate');

/**
 * Module exports
 * @public
 */
module.exports = [
    /** Create Card */
    {
        path: '/me/cards',
        method: 'post',
        middlewares: [authenticate, validation.createCard],
        controller: controller.createCard,
    },
];
