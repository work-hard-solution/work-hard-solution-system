const { validate, Joi, Segments } = require('../../core/validation');

module.exports = {
    createCard: validate({
        [Segments.BODY]: Joi.object({
            token: Joi.string().required(),
            is_primary: Joi.boolean(),
        }),
    }),
};
