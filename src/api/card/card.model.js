const mongoose = require('mongoose');
const { transformMongooseDocumentToObject: transform } = require('../../utils/utils');

/**
 * Clients schema
 * @private
 */
const Schema = new mongoose.Schema(
    {
        owner_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        customer_ref_id: { type: String, unique: true },
        gateway_account_id: { type: String },
        reference_id: { type: String },
        payment_method: { type: String },
        token: { type: String },
        last_number: { type: Number },
        exp_month: { type: Number },
        exp_year: { type: Number },
        billing_zip: { type: Number },
        is_primary: { type: Boolean, default: true },
        card_info: { type: Object },
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } },
);

/**
 * Methods
 */
Schema.method({});

/**
 * Statics
 */
Schema.statics = {
    /**
     * Get card by id
     * @param {*} id
     */
    async getById(id) {
        if (!mongoose.Types.ObjectId.isValid(id)) return undefined;
        const card = await this.findOne({ _id: id }).lean();
        return transform(card);
    },

    /**
     * Create a new card
     * @param {Object} cardDTO
     */
    async createClient(cardDTO) {
        const card = await this.create(cardDTO);
        return transform(card.toObject());
    },
};

/**
 * Module exports
 * @public
 */
module.exports = mongoose.model('Card', Schema, 'cards');
