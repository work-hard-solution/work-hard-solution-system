const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */
module.exports = Object.freeze({
    CardAlreadyExists: {
        message: 'Card already exists',
        code: 3001,
        status: httpStatus.CONFLICT,
        isPublic: true,
    },
});
