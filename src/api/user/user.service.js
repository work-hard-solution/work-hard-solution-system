const UserModel = require('mongoose').model('User');
const _ = require('lodash');
const { AppError } = require('../../core/error');
const UserError = require('./user.error');

const userTransform = [
    'id',
    'name',
    'email',
    'email_verified',
    'type',
    'picture',
    'time_zone',
    'phone_number',
    'token_fcm',
];

exports.getUserInfo = async (id) => {
    const user = await UserModel.findOne({ _id: id });
    if (_.isNull(user)) throw new AppError(UserError.UserNotFound);
    return _.pick(user, userTransform);
};
