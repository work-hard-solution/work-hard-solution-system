const authenticate = require('../../core/middleware/authenticate');
const controller = require('./user.controller');

/**
 * Module exports
 * @public
 */
module.exports = [
    {
        path: '/users',
        method: 'get',
        middlewares: [authenticate],
        controller: controller.getUserInfo,
    },
];
