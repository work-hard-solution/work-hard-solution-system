const { validate, Joi, Segments } = require('../../core/validation');
const { objectIdPattern } = require('../../utils/constant');

module.exports = {
    getUserById: validate({
        [Segments.PARAMS]: Joi.object({
            user_id: Joi.string().regex(objectIdPattern),
        }),
    }),
};
