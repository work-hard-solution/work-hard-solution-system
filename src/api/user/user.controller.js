const httpStatus = require('http-status');
const UserService = require('./user.service');
/**
 * Get current user
 */
exports.getUserInfo = async (req, res, next) => {
    try {
        const user = await UserService.getUserInfo(req.auth.id);
        return res.status(httpStatus.OK).json(user);
    } catch (err) {
        return next(err);
    }
};
