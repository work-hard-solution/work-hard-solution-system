const httpStatus = require('http-status');

/**
 * Module exports
 * @public
 */
module.exports = Object.freeze({
    UserNotFound: {
        message: 'User not found',
        code: 5001,
        status: httpStatus.NOT_FOUND,
        isPublic: true,
    },
});
