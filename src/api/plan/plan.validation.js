const { validate, Joi, Segments } = require('../../core/validation');

module.exports = {
    createPlan: validate({
        [Segments.BODY]: Joi.object({
            name: Joi.string().required(),
            price_per_member: Joi.number().required(),
            duration: Joi.number().required(),
            free_trial_days: Joi.number(),
            currency: Joi.string(),
            description: Joi.string(),
            sort_order: Joi.number(),
        }),
    }),
};
