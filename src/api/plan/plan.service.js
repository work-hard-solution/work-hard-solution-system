// Library
const _ = require('lodash');

// Model
const PlanModel = require('mongoose').model('Plan');

// Services
const chargebee = require('../../core/services/payments/chargebee');

// Error
const { AppError } = require('../../core/error');
const PlanError = require('./plan.error');

const planTransform = [
    'id',
    'plan_ref_id',
    'name',
    'description',
    'price_per_member',
    'currency',
    'duration',
    'free_trial_days',
];

/**
 * Create plan
 * @param {Object} planDTO
 */
exports.createPlan = async (planDTO) => {
    const planChargebeeDto = {
        id: planDTO.name,
        pricing_model: 'per_unit',
        currency_code: planDTO.currency || 'USD',
        invoice_name: planDTO.name,
        name: planDTO.name,
        price: planDTO.price_per_member,
        period_unit: 'day',
        ...(planDTO.free_trial_days && { trial_period_unit: 'day' }),
        trial_period: planDTO.free_trial_days,
        period: planDTO.duration,
        description: planDTO.description,
    };
    if (await PlanModel.findOne({ name: planDTO.name })) throw new AppError(PlanError.PlanAlreadyExist);

    // Create plan on chargebee
    const planChargebee = await chargebee.plan.create(planChargebeeDto).request();

    // Create plan on database
    if (planChargebee.plan) {
        const newPlan = await PlanModel.createPlan({
            ...planDTO,
            sort_order: await PlanModel.countDocuments(),
            plan_ref_id: planChargebee.plan.id,
        });

        const transformedPlan = _.pick(newPlan, planTransform);
        return transformedPlan;
    }
};
