const controller = require('./project.controller');
const validation = require('./project.validation');
const authenticate = require('../../core/middleware/authenticate');

/**
 * Module exports
 * @public
 */
module.exports = [
    /** Get project by id */
    {
        path: '/organizations/:organization_id/projects/:project_id',
        method: 'get',
        middlewares: [authenticate, validation.getProjectById],
        controller: controller.getProjectById,
    },

    /** Delete project */
    {
        path: '/organizations/:organization_id/projects/:project_id',
        method: 'delete',
        middlewares: [authenticate, validation.deleteProject],
        controller: controller.deleteProject,
    },
];
