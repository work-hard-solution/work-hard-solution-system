const request = require('supertest');
const mongoose = require('../../../core/mongoose');
const app = require('../../../core/express');
const logger = require('../../../core/logger');
const { APP_PORT, APP_HOST, APP_ENV, MONGO } = require('../../../config/config');
const TestUtilities = require('../../../utils/testUtilities');

logger.level = 'off';

// Admin
const adminDto = {
    email: 'adminUser@gmail.com',
    password: '123456',
    name: 'The Nice',
    type: 'admin',
};

// Organization
const organizationDto = {
    name: 'The Nice Group',
    picture: 'Beautiful picture',
    address: '4s Linh Dong quan Thu Duc',
    phone_number: '0356245247',
    tax_number: '123956734',
    industry: 'IT',
    team_size_from: 30,
    team_size_to: 50,
    contact: {
        name: 'CEO',
        email: 'adminUser@gmail.com',
        phone_number: '035624524443',
    },
};

describe('Organizations API Successful', () => {
    let dbConnection = '';
    let server = {};
    let admin = {};
    let organization = {};

    beforeAll(async () => {
        try {
            jest.setTimeout(30000);
            // Connect MongoDB
            dbConnection = await mongoose.connect(MONGO.URI);
            logger.info(`✅ Database initialized... (${dbConnection.name})`);
            // Run express app
            server = app.listen(APP_PORT);
            logger.info(`✅ Server listened at ${APP_HOST}:${APP_PORT} (${APP_ENV})`);
        } catch (err) {
            logger.error(err);
            process.exit(1);
        }
        try {
            // Login  with admin role
            [admin] = await Promise.all([TestUtilities.createUser(adminDto)]);
            organization = await TestUtilities.createOrganizationByCreatedById(admin.user.id, organizationDto);
        } catch (err) {
            await deleteData();
        }
    });

    // Close when done
    afterAll(async (done) => {
        await deleteData();
        server.close();
        done();
    });

    const deleteData = () =>
        Promise.all([
            TestUtilities.deleteUser([admin.user.id]),
            TestUtilities.deleteOrganization(admin.user.id, organization.id),
        ]);

    /** GET ORGANIZATIONS BY ID */
    describe('GET /v1/organizations/:organization_id', () => {
        it('should return organization with id as params', async () => {
            const response = await request(server)
                .get(`/v1/organizations/${organization.id}`)
                .set('Authorization', `Bearer ${admin.tokens.access_token}`);
            expect(response.statusCode).toBe(200);
            expect(response.body.id).toBe(organization.id);
        });
    });

    /** CREATES A ORGANIZATION */
    describe('POST /v1/organizations', () => {
        // eslint-disable-next-line global-require
        const OrganizationModel = require('mongoose').model('Organization');
        let id = '';

        // Clear record after created
        afterAll(async () => {
            if (id) await OrganizationModel.findByIdAndRemove(id).exec();
        });

        it('should return new organization ', async () => {
            const response = await request(server)
                .post(`/v1/organizations`)
                .set('Authorization', `Bearer ${admin.tokens.access_token}`)
                .send({
                    name: 'Create The Nice Group',
                    picture: 'Beautiful picture',
                    address: '151 Dao Duy Anh phuong 9 quan Phu Nhuan',
                    phone_number: '0356245247',
                    tax_number: '123956734',
                    industry: 'IT',
                    team_size_from: 30,
                    team_size_to: 50,
                    contact: {
                        name: 'CEO',
                        email: 'createOrganizationTest@gmail.com',
                        phone_number: '035624524443',
                    },
                });
            id = response.body.id;
            expect(response.statusCode).toBe(201);
        });
    });

    /** UPDATE INFO ORGANIZATION */
    describe('PUT /v1/organizations/:organization_id', () => {
        it('should return updated info organization', async () => {
            const response = await request(server)
                .put(`/v1/organizations/${organization.id}`)
                .set('Authorization', `Bearer ${admin.tokens.access_token}`)

                // All field can be update
                .send({
                    name: 'Update Info Organization',
                    picture: 'Beautiful picture',
                    address: '151 Dao Duy Anh phuong 9 quan Phu Nhuan',
                    phone_number: '0356245247',
                    tax_number: '123956734',
                    industry: 'IT',
                    team_size_from: 30,
                    team_size_to: 50,
                    contact: {
                        name: 'CEO',
                        email: 'luuphuc6297@gmail.com',
                        phone_number: '035624524443',
                    },
                });
            expect(response.statusCode).toBe(200);
            expect(response.body.name).toBe('UpdateInfoOrganization');
        });
    });

    /** DELETE ORGANIZATION */
    describe('DELETE /v1/organizations/:organization_id', () => {
        it('should return no content', async () => {
            const response = await request(server)
                .delete(`/v1/organizations/${organization.id}`)
                .set('Authorization', `Bearer ${admin.tokens.access_token}`);
            expect(response.statusCode).toBe(204);
        });
    });
});
