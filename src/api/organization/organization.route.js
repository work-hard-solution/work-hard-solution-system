const controller = require('./organization.controller');
const validation = require('./organization.validation');
const authenticate = require('../../core/middleware/authenticate');

/**
 * Module exports
 * @public
 */
module.exports = [
    /** Get organization by id */
    {
        path: '/organizations/:organization_id',
        method: 'get',
        middlewares: [authenticate, validation.getOrganizationById],
        controller: controller.getOrganizationById,
    },

    /** Creates a organization */
    {
        path: '/organizations',
        method: 'post',
        middlewares: [authenticate, validation.createOrganization],
        controller: controller.createOrganization,
    },

    /** Update info organization */
    {
        path: '/organizations/:organization_id',
        method: 'put',
        middlewares: [authenticate, validation.updateInfoOrganization],
        controller: controller.updateInfoOrganization,
    },

    /** Delete organization */
    {
        path: '/organizations/:organization_id',
        method: 'delete',
        middlewares: [authenticate, validation.deleteOrganization],
        controller: controller.deleteOrganization,
    },
];
