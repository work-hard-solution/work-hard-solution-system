const mongoose = require('mongoose');
const { transformMongooseDocumentToObject: transform } = require('../../utils/utils');
const { ORGANIZATION_STATUS } = require('../../utils/constants');

/**
 * Organization schema
 * @private
 */
const Schema = new mongoose.Schema(
    {
        created_by_id: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        name: { type: String },
        picture: { type: String },
        address: { type: String },
        phone_number: { type: String },
        tax_number: { type: String },
        industry: { type: String },
        team_size_from: { type: Number },
        team_size_to: { type: Number },
        contact: {
            name: { type: String },
            email: { type: String },
            phone_number: { type: String },
        },
        status: { type: String, enum: Object.values(ORGANIZATION_STATUS), default: ORGANIZATION_STATUS.ACTIVE },
        invite_url: { type: String },
    },
    { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } },
);

/**
 * Index fields
 */
Schema.index({ name: 'text' });

/**
 * Methods
 */
Schema.method({});

/**
 * Statics
 */
Schema.statics = {
    /**
     * Get organization by id
     * @param {*} id
     */
    async getById(id) {
        if (!mongoose.Types.ObjectId.isValid(id)) return undefined;
        const organization = await this.findOne({ _id: id }).lean();
        return transform(organization);
    },

    /**
     * Create a new organization
     * @param {Object} organizationDTO
     */
    async createOrganization(organizationDTO) {
        const organization = await this.create(organizationDTO);
        return transform(organization.toObject());
    },
};

/**
 * Module exports
 * @public
 */
module.exports = mongoose.model('Organization', Schema, 'organizations');
