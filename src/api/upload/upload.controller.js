const httpStatus = require('http-status');

// Library
const _ = require('lodash');

// Config awsS3
const { multerImage } = require('../../config/multer');

// Error
const { AppError } = require('../../core/error');
const UploadError = require('./upload.error');

// Multer single upload
const upload = multerImage.single('image');

/**
 * Upload image
 */
exports.uploadImage = (req, res, next) => {
    upload(req, res, (err) => {
        if (err) {
            return next(err);
        }
        // Check exists image
        if (_.isNull(req.file)) return next(new AppError(UploadError.ImageNotFound));

        return res.status(httpStatus.OK).json({
            url: req.file.location,
        });
    });
};
