const path = require('path');
const multer = require('multer');

// Config
const { s3Storage } = require('../services/awsS3');

// Constants
const { LIMIT_IMAGE_SIZE } = require('../utils/constants');

// Error
const { AppError, errors } = require('../core/error');

// Set storage
const storage = s3Storage;

// Image limit configs
const imageLimits = { fileSize: LIMIT_IMAGE_SIZE };

// Image filter
const imageFilter = (req, file, cb) => {
    const fileTypes = /jpeg|jpg|png|bmp/;
    const mimeType = fileTypes.test(file.mimetype);
    const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());

    if (mimeType && extName) {
        return cb(null, true);
    }
    const err = new AppError(errors.UnsupportedTypeImage);
    cb(err);
};

exports.multerImage = multer({ storage, limits: imageLimits, fileFilter: imageFilter });
