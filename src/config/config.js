const dotenv = require('dotenv');

// Loads ".env" file contents into "process.env"
require('dotenv').config({ path: './.env.default' });

// Overwrite env config
if (process.env.NODE_ENV) {
    // eslint-disable-next-line no-undef
    const envConfig = dotenv.parse(fs.readFileSync(`./.env.${process.env.NODE_ENV}`));
    Object.keys(envConfig).forEach((key) => {
        process.env[key] = envConfig[key];
    });
}

module.exports = {
    APP_ENV: process.env.NODE_ENV || 'local',
    APP_NAME: process.env.APP_NAME,
    APP_PORT: process.env.APP_PORT,
    APP_HOST: process.env.APP_HOST,
    JWT_SECRET: process.env.JWT_SECRET,
    ACCESS_TOKEN_EXP: process.env.ACCESS_TOKEN_EXP,
    REFRESH_TOKEN_EXP: process.env.REFRESH_TOKEN_EXP,
    RESET_PASSWORD_TOKEN_EXP: process.env.RESET_PASSWORD_TOKEN_EXP,
    VERIFY_ACCOUNT_TOKEN: process.env.VERIFY_ACCOUNT_TOKEN,
    FORGOT_PASSWORD_CODE_EXP: process.env.FORGOT_PASSWORD_CODE_EXP,
    MONGO: {
        URI: process.env.MONGO_URI,
    },
    REDIS_URL: process.env.REDIS_URL,
    // Brain Tree
    BRAINTREE_MERCHANID: process.env.BRAINTREE_MERCHANID,
    BRAINTREE_PUBLIC_KEY: process.env.BRAINTREE_PUBLIC_KEY,
    BRAINTREE_PRIVATE_KEY: process.env.BRAINTREE_PRIVATE_KEY,

    // Stripe
    STRIPE_PUBLISHABLE_KEY: process.env.STRIPE_PUBLISHABLE_KEY,
    STRIPE_SECRET_KEY: process.env.STRIPE_SECRET_KEY,

    // Chargebee
    CHARGEBEE_API_KEY: process.env.CHARGEBEE_API_KEY,
    CHARGEBEE_SITE: process.env.CHARGEBEE_SITE,
    CHARGEBEE_GATEWAY_ACCOUNT_ID: process.env.CHARGEBEE_GATEWAY_ACCOUNT_ID,

    // AWS-S3
    AWS_ACCESS_KEY_ID: process.env.AWS_ACCESS_KEY_ID,
    AWS_SECRET_KEY: process.env.AWS_SECRET_KEY,
    AWS_REGION: process.env.AWS_REGION,
    AWS_S3_UPLOAD_BUCKET: process.env.AWS_S3_UPLOAD_BUCKET || '',
};
